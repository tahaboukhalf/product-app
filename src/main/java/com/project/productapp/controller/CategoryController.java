package com.project.productapp.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.productapp.exceptions.ResourceNotFoundException;
import com.project.productapp.model.Category;
import com.project.productapp.repository.CategoryRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "http://localhost:3000")
@Api("API for CRUD operations on product.")
@RestController
@RequestMapping("/")
public class CategoryController {
	
	
	
    private CategoryRepository categoryRepository;
	
	
	@Autowired
	public CategoryController(@RequestBody CategoryRepository categoryRepository) {
		this.categoryRepository = categoryRepository;
	}
	
	
	@ApiOperation(value = "Get All categories")
	@GetMapping("/category")
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }
	
	@ApiOperation(value = "Get a category by its Id")
	@GetMapping("/category/{categoryId}")
    public Optional<Category> getCategoriesById(@PathVariable Long categoryId) {
        return categoryRepository.findById(categoryId);
    }
	
	
	@ApiOperation(value = "Add a new category")
	@PostMapping("/category")
    public Category createCategory(@Valid @RequestBody Category category) {
        return categoryRepository.save(category);
    }
	
	
	@ApiOperation(value = "Modify a category by it's Id")
	@PutMapping("/category/{categoryId}")
    public Category updateCategory(@PathVariable Long categoryId, @Valid @RequestBody Category categoryRequest) {
        return categoryRepository.findById(categoryId).map(category -> {
            category.setName(categoryRequest.getName());
            return categoryRepository.save(category);
        }).orElseThrow(() -> new ResourceNotFoundException("categoryId " + categoryId + " not found"));
    }
	
	@ApiOperation(value = "Delete a category by it's Id")
	@DeleteMapping("/category/{categoryId}")
    public ResponseEntity<?> deleteCategory(@PathVariable Long categoryId) {
        return categoryRepository.findById(categoryId).map(category -> {
            categoryRepository.delete(category);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("CategoryId " + categoryId + " not found"));
    }
	

}























