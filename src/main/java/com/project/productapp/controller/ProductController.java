package com.project.productapp.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.productapp.exceptions.ResourceNotFoundException;
import com.project.productapp.model.Category;
import com.project.productapp.model.Product;
import com.project.productapp.repository.CategoryRepository;
import com.project.productapp.repository.ProductRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "http://localhost:3000")
@Api("API for CRUD operations on product.")
@RestController
@RequestMapping("/category")
public class ProductController {

	
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired(required = true)
	public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
	
	

	@ApiOperation(value = "Get the list of all product by their category")
	@GetMapping("/{categoryId}/product")
	public List<Product> getAllProductsByCategorytId(@PathVariable(value = "categoryId") Long categoryId) {
		
		return productRepository.findByCategoryId(categoryId);
	}
	
	@ApiOperation(value = "Get a product by its Id and its category")
	@GetMapping("/{categoryId}/product/{productId}")
	public Optional<Product> getProductsByProducttId(@PathVariable(value = "productId") Long productId,
			@PathVariable(value = "categoryId") Long categoryId) {
		return productRepository.findByIdAndCategoryId(productId,categoryId);
	}
	
	@ApiOperation(value = "Add a product in the chosen category")
	@PostMapping("/{categoryId}/product")
    public Product createProduct(@PathVariable(value = "categoryId") Long categoryId, @Valid @RequestBody Product product) {
//		Optional<Category> tt = categoryRepository.findById(categoryId); 
		
		
//		return productRepository.save(product);
		
	
		
		 return categoryRepository.findById(categoryId).map(p -> {
	            product.setCategory(p);
	            return productRepository.save(product);
	        }).orElseThrow(() -> new ResourceNotFoundException("CategoryId " + categoryId + "not found"));
		 
	      }
	
	@ApiOperation(value = "Modify a product by its Id and its category")
	@PutMapping("/{categoryId}/product/{productId}")
    public Product updateProduct(@PathVariable (value = "categoryId") Long categoryId,
                                 @PathVariable (value = "productId") Long productId,
                                 @RequestBody @Valid  Product productRequest) {
        if(!categoryRepository.existsById(categoryId)) {
            throw new ResourceNotFoundException("CategoryId " + categoryId + " not found");
        }

        return productRepository.findByIdAndCategoryId(productId, categoryId).map(product -> {
            product.setName(productRequest.getName());
            product.setPrice(productRequest.getPrice());
            return productRepository.save(product);
        }).orElseThrow(() -> new ResourceNotFoundException("ProductId " + productId + "not found"));
    }
	
	
	@ApiOperation(value = "Delete a product by its Id and its category")
	@DeleteMapping("/{categoryId}/product/{productId}")
    public ResponseEntity<?> deleteProduct(@PathVariable (value = "categoryId") Long categoryId,
                              @PathVariable (value = "productId") Long productId) {
        return productRepository.findByIdAndCategoryId(productId, categoryId).map(product -> {
            productRepository.delete(product);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Product not found with id " + productId + " and categoryId " + categoryId));
    }

}














