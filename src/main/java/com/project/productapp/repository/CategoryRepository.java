package com.project.productapp.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.productapp.model.Category;
import com.project.productapp.model.Product;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
