package com.project.productapp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import com.project.productapp.model.Category;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductappApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoryControllerIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@LocalServerPort
	private int port;
	
	
	private String getRootUrl() {
		return "http://localhost:" + port ;
	}
	
	@Test
	public void contextLoads() {
		
	}
	
	@Test
	public void testGetAllCategories() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/category",
        HttpMethod.GET, entity, String.class);  
        assertNotNull(response.getBody());
	}
	
	  @Test
	   public void testUpdateCategory() {
	     int id = 1;
	     Category category = restTemplate.getForObject(getRootUrl() + "/category/" + id, Category.class);
	     category.setName("category1");
	     restTemplate.put(getRootUrl() + "/category/" + id, category);
	     Category updatedCategory = restTemplate.getForObject(getRootUrl() + "/category/" + id, Category.class);
	     assertNotNull(updatedCategory);
	     }
	
	   @Test
	    public void testDeleteCategory() {
	      int id = 2;
	      Category category = restTemplate.getForObject(getRootUrl() + "/category/" + id, Category.class);
	      assertNotNull(category);
          restTemplate.delete(getRootUrl() + "/category/" + id);
          	try {
          		category = restTemplate.getForObject(getRootUrl() + "/category/" + id, Category.class);
	          } catch (final HttpClientErrorException e) {
	               assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
	          }
	     }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
